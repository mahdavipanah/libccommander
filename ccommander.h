#ifndef __CCOMMANDER_H__
#define __CCOMMANDER_H__

#include <stddef.h>
#include <stdbool.h>

/*
 * Error macro definitions
 */

#define CC_FAILURE_MALLOC 1
#define CC_FAILURE_COMMAND_NOT_FOUND 2
#define CC_FAILURE_OPTION_NOT_FOUND 3
#define CC_FAILURE_COMMAND_REQUIRED_ARGS 4
#define CC_FAILURE_NULL_CC_PASSED 5

/*
 * Struct definitions
 */

struct CC_arg
{
        char *name;
        char *value;
        bool optional;
};
struct CC_option
{
        char          *long_str;
        char          *short_str;
        char          *description;
        bool isset;

        struct CC_arg* args;
        size_t argslen;
};

struct CCommander_t;
typedef struct CCommander_t* CCommander;

struct CCommand
{
        char *str;
        char *description;
        int (*function)(CCommander*);
};

struct CC_command
{
        char          *str;
        char          *description;

        struct CC_arg* args;
        size_t argslen;

        int (*function)(CCommander*);
};

struct CCommander_t {
        char              *name;
        char              *binname;

        char              *version;
        struct CC_option  version_option;

        struct CC_option  help_option;


        struct CC_option  *options;
        size_t optionslen;

        struct CC_command *commands;
        size_t commandslen;

        int (*default_command)(CCommander*);
};

/*
 * Function declarations
 */

void        cc_aterror                   (void (*)(int));
const char* cc_errno_msg                 (int);
void        cc_perror                    (int);

void        cc_init                      (CCommander*);
void        cc_set_default_command       (CCommander*,
                                          int (*)(CCommander*));
void        cc_set_default_command_alias (CCommander*,
                                          const char*);
void        cc_version                   (CCommander*,
                                          const char*);
void        cc_name                      (CCommander*,
                                          const char*);
void        cc_options                   (CCommander*,
                                          size_t,
                                          const char*[][3]);
void        cc_commands                  (CCommander*,
                                          size_t,
                                          const struct CCommand*);
void        cc_free                      (CCommander*);
int         cc_parse                     (CCommander*,
                                          int,
                                          const char*[]);
bool        cc_isset_option              (CCommander*,
                                          const char*);
const char* cc_get_option                (CCommander*,
                                          const char*,
                                          const char*);
const char* cc_get_command               (CCommander*,
                                          const char*,
                                          const char*);
void        cc_print_help                (CCommander*);
void        cc_help_option               (CCommander*,
                                          const char*,
                                          const char*,
                                          const char*);
void        cc_version_option               (CCommander*,
                                          const char*,
                                          const char*,
                                          const char*);

#endif /* __CCOMMANDER_H__ */
