#include "ccommander.h"
#include <string.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>

/*
 * Macro definitions for printing colored output
 */
#define CC_ANSI_COLOR_RED     "\x1b[31m"
#define CC_ANSI_COLOR_GREEN   "\x1b[32m"
#define CC_ANSI_COLOR_YELLOW  "\x1b[33m"
#define CC_ANSI_COLOR_BLUE    "\x1b[34m"
#define CC_ANSI_COLOR_RESET   "\x1b[0m"

/*
 * Macro definition for printing error outputs
 */
#define CC_print_app_error(CC, format, ...) fprintf(stderr,"%s: " CC_ANSI_COLOR_RED "error: " CC_ANSI_COLOR_RESET format, CC->name, __VA_ARGS__)

/*
 * Function pointer for storing error handler.
 */
static void (*CC_error_handler)(int) = NULL;

/*
 * Declaration of all static functions and struct in this file
 */

static size_t             CC_count_char                 (const char*,
                                                         const char);
static size_t             CC_first_char_index           (const char*,
                                                         const char);
static void               CC_error                      (int);
static void               CC_free_arg_array             (struct CC_arg**,
                                                         size_t*);
static void               CC_free_option_array          (struct CC_option**,
                                                         size_t*);
static void               CC_free_command_array         (struct CC_command**,
                                                         size_t *);
static int                CC_check_init                 (CCommander*);
static void               CC_parse_str_to_args          (const char*,
                                                         struct CC_arg**,
                                                         size_t*);
static struct CC_command* CC_has_command                (CCommander*,
                                                         const char*);
static struct CC_command* CC_has_command_by_function    (CCommander*,
                                                         int (*)(CCommander*));
static struct CC_option*  CC_has_option                 (CCommander*,
                                                         const char*);
static size_t             CC_command_require_args_count (struct CC_command*);
static size_t             CC_parse_command_arguments    (struct CC_command*,
                                                         int,
                                                         const char**);
static size_t             CC_option_require_args_count   (struct CC_option*);
static size_t             CC_parse_option_arguments      (struct CC_option*,
                                                          int,
                                                          const char**);
static void               CC_print_help                  (CCommander*);

/*
 *  String utilities
 */

static size_t
CC_count_char (const char *str,
               const char  c)
{
        size_t char_counts = 0;
        for (; *str; ++str)
                if (*str == c)
                        ++char_counts;
        return char_counts;
}

// Returns -1 if not found
static size_t
CC_first_char_index (const char  *str,
                     const char   c )
{
        const char * first_char = str;
        for (; *str; ++str)
                if (*str == c)
                        return str - first_char;
        return -1;
}

/*
 *  Error handling
 */
// Sets error handler function
void
cc_aterror (void (*err_handler)(int))
{
        CC_error_handler = err_handler;
}
// Returns appropriate message related to error number
const char *
cc_errno_msg (int errno)
{
        switch (errno) {
        case CC_FAILURE_MALLOC:
                return "Failure in memory allocation";

        case CC_FAILURE_COMMAND_NOT_FOUND:
                return "No such command has been defined";

        case CC_FAILURE_OPTION_NOT_FOUND:
                return "No such option has been defined";

        case CC_FAILURE_COMMAND_REQUIRED_ARGS:
                return "Submited command should not have any required arguments";

        case CC_FAILURE_NULL_CC_PASSED:
                return "NULL CCommander (cc) passed as function argument";
        }
        return "";
}
// Prints a custom message with errorno's message to standard error
void
cc_perror (int errno)
{
        fprintf(stderr, "libccommander: " CC_ANSI_COLOR_RED "error" CC_ANSI_COLOR_RESET ": %s.\n", cc_errno_msg(errno));
}
// Calls error handler with error no and it's message
static void
CC_error(int errno)
{
        if (CC_error_handler != NULL)
                CC_error_handler(errno);
}


/*
 * Functions to free memory allocations for structures
 */

static void
CC_free_arg_array (struct CC_arg **args,
                   size_t         *argslen)
{
        if (*args == NULL || *argslen == 0)
                return;
        for (size_t i = 0; i < *argslen; ++i) {
                free((*args)[i].name);
                free((*args)[i].value);
        }
        free(*args);
        *args = NULL;
        *argslen = 0;
}

static void
CC_free_option_array (struct CC_option **opts,
                     size_t            *optslen)
{
        if (*opts == NULL || *optslen == 0)
                return;
        for (size_t i = 0; i < *optslen; ++i) {
                free((*opts)[i].short_str);
                free((*opts)[i].long_str);
                free((*opts)[i].description);
                CC_free_arg_array(&((*opts)[i].args), &((*opts)[i].argslen));
        }
        free(*opts);
        *opts = NULL;
        *optslen = 0;
}

static void
CC_free_command_array (struct CC_command **cmds,
                      size_t             *cmdslen)
{
        if (*cmds == NULL || *cmdslen == 0)
                return;
        for (size_t i = 0; i < *cmdslen; ++i) {
                free((*cmds)[i].str);
                free((*cmds)[i].description);
                CC_free_arg_array(&((*cmds)[i].args), &((*cmds)[i].argslen));
        }
        free(*cmds);
        *cmds = NULL;
        *cmdslen = 0;
}

static void CC_free_option(struct CC_option *opt) {
        if (opt == NULL)
                return;
        if (opt->short_str != NULL)
                free (opt->short_str);
        if (opt->long_str != NULL)
                free (opt->long_str);
        if (opt->description != NULL)
                free (opt->description);
}

void
cc_free (CCommander * CC)
{
        if (*CC == NULL)
                return;
        if ((*CC)->name != NULL)
                free ((*CC)->name);
        if ((*CC)->version != NULL)
                free ((*CC)->version);
        CC_free_option_array(&((*CC)->options), &((*CC)->optionslen));
        CC_free_command_array(&((*CC)->commands), &((*CC)->commandslen));

        CC_free_option(&((*CC)->help_option));
        CC_free_option(&((*CC)->version_option));
        free(*CC);
        *CC = NULL;
}

void
cc_version_option (CCommander *CC,
                   const char *short_str,
                   const char *long_str,
                   const char *description)
{
        if (*CC == NULL) {
                CC_error(CC_FAILURE_NULL_CC_PASSED);
                return;
        }
        char *shortstr       = malloc(sizeof(char) * strlen(short_str) + 1),
             *longstr        = malloc(sizeof(char) * strlen(long_str) + 1),
             *descriptionstr = malloc(sizeof(char) * strlen(description) + 1);
        if (shortstr       == NULL ||
            longstr        == NULL ||
            descriptionstr == NULL) {
                    CC_error(CC_FAILURE_MALLOC);
                    return;
            }
        strcpy(shortstr      , short_str);
        strcpy(longstr       , long_str);
        strcpy(descriptionstr, description);

        CC_free_option(&((*CC)->version_option));

        (*CC)->version_option.short_str   = shortstr;
        (*CC)->version_option.long_str    = longstr;
        (*CC)->version_option.description = descriptionstr;
}

void
cc_help_option (CCommander *CC,
                   const char *short_str,
                   const char *long_str,
                   const char *description)
{
        if (*CC == NULL) {
                CC_error(CC_FAILURE_NULL_CC_PASSED);
                return;
        }
        char *shortstr       = malloc(sizeof(char) * strlen(short_str) + 1),
             *longstr        = malloc(sizeof(char) * strlen(long_str) + 1),
             *descriptionstr = malloc(sizeof(char) * strlen(description) + 1);
        if (shortstr       == NULL ||
            longstr        == NULL ||
            descriptionstr == NULL) {
                    CC_error(CC_FAILURE_MALLOC);
                    return;
            }
        strcpy(shortstr      , short_str);
        strcpy(longstr       , long_str);
        strcpy(descriptionstr, description);

        CC_free_option(&((*CC)->help_option));

        (*CC)->help_option.short_str   = shortstr;
        (*CC)->help_option.long_str    = longstr;
        (*CC)->help_option.description = descriptionstr;
}

/*
 *  Initializes memory for NULL CCommander pointer (cc)
 *  Errors:
 *      CC_FAILURE_MALLOC: Problem in allocating memory
 */
void
cc_init (CCommander* CC)
{
        if (*CC == NULL)
                *CC = malloc(sizeof(struct CCommander_t));
        else
                return;
        // Checks if memory allocation failed
        if (*CC == NULL)
                return CC_error(CC_FAILURE_MALLOC);
        CCommander cc = *CC;
        cc->name      = NULL;
        cc->binname   = NULL;

        cc->version = NULL;
        cc_version_option (CC, "-v", "--version", "Output the version number");
        cc->version_option.argslen = 0;
        cc->version_option.args    = NULL;

        cc_help_option    (CC, "-h", "--help",    "Output usage information");
        cc->help_option.argslen = 0;
        cc->help_option.args    = NULL;

        cc->options    = NULL;
        cc->optionslen = 0;

        cc->commands    = NULL;
        cc->commandslen = 0;

        cc->default_command = NULL;
}

/*
 *  Returns:
 *      1: If everything is OK
 *      0: If some problem happened
 */
static int
CC_check_init (CCommander* CC)
{
        // Check if CC is not initialized yet;
        if (*CC == NULL)
                cc_init(CC);
        if (*CC == NULL)
                return 0;
        return 1;
}

/*
 * Sets version string for cc
 *
 * Invokes cc_init if cc is NULL
 *
 * Errors:
 *      CC_FAILURE_MALLOC: if failed because of memory allocation failure
 */
void
cc_version (CCommander *CC,
            const char *version)
{
        if (!CC_check_init(CC))
                return;
        CCommander cc = *CC;
        size_t version_strlen = strlen(version);
        if (cc->version == NULL)
                cc->version = malloc(sizeof(char) * version_strlen + 1);
        else
        if (strlen(cc->version) != version_strlen) {
                free(cc->version);
                cc->version = malloc(sizeof(char) * version_strlen + 1);
                if (cc->version == NULL) {
                        CC_error(CC_FAILURE_MALLOC);
                        return;
                }
        }
        strcpy(cc->version, version);
}

/*
 * Sets name string for cc
 *
 * Invokes cc_init if cc is NULL
 *
 * Errors:
 *      CC_FAILURE_MALLOC: if failed because of memory allocation failure
 */
void
cc_name (CCommander *CC,
         const char *name) {
        if (!CC_check_init(CC))
                return;
        CCommander cc = *CC;
        size_t name_strlen = strlen(name);
        if (cc->name == NULL)
                cc->name = malloc(sizeof(char) * name_strlen + 1);
        else
        if (strlen(cc->name) != name_strlen) {
                free(cc->name);
                cc->name = malloc(sizeof(char) * name_strlen + 1);
                if (cc->name == NULL) {
                        CC_error(CC_FAILURE_MALLOC);
                        return;
                }
        }
        strcpy(cc->name, name);
}


static void
CC_parse_str_to_args (const char     *str,
                     struct CC_arg **args,
                     size_t         *argslen)
{
        if (*args != NULL)
                CC_free_arg_array(args, argslen);
        *argslen = CC_count_char(str, '<') + CC_count_char(str, '[');
        if (*argslen == 0)
                return;
        *args = malloc(sizeof(struct CC_arg) * (*argslen));
        size_t index_lt = CC_first_char_index(str, '<');
        int i;
        const char * temp_str = str + index_lt + 1;
        for (i = 0; index_lt != -1; ++i) {
                size_t index_gt = CC_first_char_index(temp_str, '>');
                (*args)[i].name = malloc(sizeof(char) * index_gt + 1);
                strncpy((*args)[i].name, temp_str, index_gt);
                (*args)[i].optional = false;
                index_lt = CC_first_char_index(temp_str, '<');
                temp_str += index_lt + 1;
        }
        size_t index_bo = CC_first_char_index(str, '[');
        temp_str = str + index_bo + 1;
        for (; index_bo != -1; ++i) {
                size_t index_bc = CC_first_char_index(temp_str, ']');
                (*args)[i].name = malloc(sizeof(char) * index_bc + 1);
                strncpy((*args)[i].name, temp_str, index_bc);
                (*args)[i].optional = true;
                index_bo = CC_first_char_index(temp_str, '[');
                temp_str += index_bo + 1;
        }
}

/*
 * Sets the options
 *
 * Invokes cc_init if cc is NULL
 *
 * Errors:
 *      CC_FAILURE_MALLOC: if failed because of memory allocation failure
 */
void
cc_options (CCommander *CC,
           size_t      len,
           const char *opts[][3])
{
        if (!CC_check_init(CC))
                return;
        CCommander cc = *CC;
        if (cc->options != NULL) {
                CC_free_option_array(&(cc->options), &cc->optionslen);
        }
        cc->options = malloc(sizeof(struct CC_option) * len);
        if (cc->options == NULL) {
                CC_error(CC_FAILURE_MALLOC);
                return;
        }
        cc->optionslen = len;
        for (size_t i = 0; i < len; ++i) {
                char *long_str, *short_str, *description;
                long_str = malloc(sizeof(char) * strlen(opts[i][0]) + 1);
                short_str = malloc(sizeof(char) * strlen(opts[i][1]) + 1);
                description = malloc(sizeof(char) * strlen(opts[i][2]) + 1);
                if (short_str == NULL || long_str == NULL || description == NULL) {
                        CC_error(CC_FAILURE_MALLOC);
                        return;
                }
                strcpy(long_str, opts[i][0]);
                strcpy(short_str, opts[i][1]);
                strcpy(description, opts[i][2]);
                cc->options[i].long_str = long_str;
                cc->options[i].short_str = short_str;
                cc->options[i].description = description;
                cc->options[i].isset = false;
                CC_parse_str_to_args(long_str, &(cc->options[i].args), &(cc->options[i].argslen) );
        }
}

/*
 * Sets the commands
 *
 * Invokes cc_init if cc is NULL
 *
 * Errors:
 *      CC_FAILURE_MALLOC: if failed because of memory allocation failure
 */
void
cc_commands (CCommander            *CC,
             size_t                 len,
             const struct CCommand *cmds)
{
        if (!CC_check_init(CC))
                return;
        CCommander cc = *CC;
        if (cc->commands != NULL) {
                CC_free_command_array(&(cc->commands), &cc->commandslen);
        }
        cc->commands = malloc(sizeof(struct CC_command) * len);
        if (cc->commands == NULL) {
                CC_error(CC_FAILURE_MALLOC);
                return;
        }
        cc->commandslen = len;
        for (size_t i = 0; i < len; ++i) {
                char *str, *description;
                str = malloc(sizeof(char) * strlen(cmds[i].str) + 1);
                description = malloc(sizeof(char) * strlen(cmds[i].description) + 1);
                if (str == NULL || description == NULL) {
                        CC_error(CC_FAILURE_MALLOC);
                        return;
                }
                strcpy(str, cmds[i].str);
                strcpy(description, cmds[i].description);
                cc->commands[i].str = str;
                cc->commands[i].description = description;
                cc->commands[i].function = cmds[i].function;
                CC_parse_str_to_args(str, &(cc->commands[i].args), &(cc->commands[i].argslen) );
        }
}

static struct CC_command*
CC_has_command (CCommander *CC,
                const char *cmd_str)
{
        for (size_t i = 0; i < (*CC)->commandslen; ++i) {
                size_t index_space = CC_first_char_index((*CC)->commands[i].str, ' ');
                if (strncmp(cmd_str, (*CC)->commands[i].str, index_space) == 0)
                        return &((*CC)->commands[i]);
        }
        return NULL;
}

static struct CC_command*
CC_has_command_by_function (CCommander *CC,
                            int (*function)(CCommander*))
{
        for (size_t i = 0; i < (*CC)->commandslen; ++i) {
                if (function == (*CC)->commands[i].function)
                        return &((*CC)->commands[i]);
        }
        return NULL;
}

static struct CC_option*
CC_has_option (CCommander *CC,
               const char *opt_str)
{
        for (size_t i = 0; i < (*CC)->optionslen; ++i) {
                size_t index_space = CC_first_char_index((*CC)->options[i].long_str, ' ');
                if (strncmp(opt_str, (*CC)->options[i].long_str, index_space) == 0
                    || strcmp(opt_str,(*CC)->options[i].short_str) == 0)
                        return &((*CC)->options[i]);
        }
        return NULL;
}

/*
 * Empty argument handling
 */
void
cc_set_default_command (CCommander *CC,
                        int (*function)(CCommander*))
{
        if (*CC == NULL) {
                CC_error(CC_FAILURE_NULL_CC_PASSED);
                return;
        }
        (*CC)->default_command = function;
}
/*
 * Sets the default command to a command with given name(cmd_str)
 *
 * Passed command should not have any required arguments.
 *
 * Errors:
 *    CC_FAILURE_COMMAND_NOT_FOUND: if passed command name has not been declared.
 *
 */
void
cc_set_default_command_alias (CCommander *CC,
                              const char *cmd_str) {
        if (*CC == NULL) {
                CC_error(CC_FAILURE_NULL_CC_PASSED);
                return;
        }
        struct CC_command * cmd = CC_has_command(CC, cmd_str);
        if (cmd == NULL) {
                CC_error(CC_FAILURE_COMMAND_NOT_FOUND);
                return;
        }
        for (size_t i = 0; i < cmd->argslen; ++i)
                if (cmd->args[i].optional == false) {
                        CC_error(CC_FAILURE_COMMAND_REQUIRED_ARGS);
                        return;
                }
        (*CC)->default_command = cmd->function;
}


static size_t
CC_command_require_args_count (struct CC_command *cmd)
{
        size_t count = 0;
        for (size_t i = 0; i < cmd->argslen; ++i)
                if (cmd->args[i].optional == false)
                        ++count;
        return count;
}

static size_t
CC_parse_command_arguments (struct CC_command *cmd,
                            int                argc,
                            const char       **argv)
{
        size_t i, require_count = CC_command_require_args_count(cmd);
        for (i = 0; i < argc && i < cmd->argslen; ++i) {
                if (argv[i][0] == '-') {
                        if (i < require_count)
                                return -1;
                        return i;
                }
                cmd->args[i].value = malloc(sizeof(char) * strlen(argv[i]) + 1);
                strcpy(cmd->args[i].value, argv[i]);
                if (cmd->args[i].optional == true)
                        ++require_count;
        }
        if (i < require_count)
                return -1;
        return i;
}

static size_t
CC_option_require_args_count (struct CC_option *opt)
{
        size_t count = 0;
        for (size_t i = 0; i < opt->argslen; ++i)
                if (opt->args[i].optional == false)
                        ++count;
        return count;
}

static size_t
CC_parse_option_arguments (struct CC_option *opt,
                                         int               argc,
                                         const char      **argv)
{
        size_t i, require_count = CC_option_require_args_count(opt);
        for (i = 0; i < argc && i < opt->argslen; ++i) {
                if (argv[i][0] == '-') {
                        if (i < require_count)
                                return -1;
                        return i;
                }
                opt->args[i].value = malloc(sizeof(char) * strlen(argv[i]) + 1);
                if (opt->args[i].value == NULL) {
                        CC_error(CC_FAILURE_MALLOC);
                        continue;
                }
                strcpy(opt->args[i].value, argv[i]);
        }
        if (i < require_count)
                return -1;
        return i;
}


/*
 * Parse arguments and calls functions and returns STATUS code
 *
 * Invokes cc_init if cc is NULL
 *
 * Errors:
 *      CC_FAILURE_MALLOC: if failed because of memory allocation failure
 */
int
cc_parse (CCommander *CC,
          int         argc,
          const char *argv[])
{
        if (!CC_check_init(CC))
                return CC_FAILURE_MALLOC;
        CCommander cc = *CC;
        size_t argv0_strlen = strlen(argv[0]);
        // Checks if executable has been run in a directory. Example: ./a.out command1 --option2
        if (argv[0][0] == '.' && argv[0][1] == '/')
                argv0_strlen = strlen(argv[0] - 2);
        char argv0[argv0_strlen + 1];
        strcpy(argv0, argv[0] + 2);
        cc->binname = malloc(sizeof(char) * strlen(argv[0]) + 1);
        if (cc->binname == NULL) {
                CC_error(CC_FAILURE_MALLOC);
                return EXIT_FAILURE;
        }
        strcpy(cc->binname, argv[0]);
        // Checks if cc's name has not set
        if (cc->name == NULL) {
                /* Sets cc's name from argv's first element */
                cc->name = malloc(sizeof(char) * argv0_strlen + 1);
                if (cc->name == NULL) {
                        CC_error(CC_FAILURE_MALLOC);
                        return EXIT_FAILURE;
                }
                strcpy(cc->name, argv0);
        }
        // If executable has been run with no arguments
        if (argc < 2) {
                if (cc->default_command != NULL) {
                        int func_retval = cc->default_command(CC);
                        return func_retval;
                } else {
                        CC_print_help(CC);
                        return EXIT_SUCCESS;
                }
        }
        if (strcmp(argv[1], cc->help_option.long_str) == 0 || strcmp(argv[1], cc->help_option.short_str) == 0) {
                CC_print_help(CC);
                return EXIT_SUCCESS;
        }
        if (strcmp(argv[1], cc->version_option.long_str) == 0 || strcmp(argv[1], cc->version_option.short_str) == 0) {
                printf("%s version %s\n", cc->name, cc->version);
                return EXIT_SUCCESS;
        }
        struct CC_command * cmd = CC_has_command(CC, argv[1]);
        if (cmd == NULL && argv[1][0] != '-') {
                CC_print_app_error(cc, "'%s' is not a command. See '%s %s'.\n", argv[1], argv[0], cc->help_option.long_str);
                return EXIT_FAILURE;
        }
        size_t index = 2;
        if(cmd == NULL && argv[1][0] == '-') {
                if (cc->default_command == NULL) {
                        CC_print_help(CC);
                        return EXIT_SUCCESS;
                }
                cmd = CC_has_command_by_function(CC, cc->default_command);
                index = 1;
        }
        if (cmd != NULL) {
                size_t cmd_require_args_count = CC_command_require_args_count(cmd);
                if (cmd_require_args_count != 0) {
                        index = CC_parse_command_arguments(cmd, argc - 2, argv + 2);
                        if (index == -1) {
                                CC_print_app_error(cc, "Command needs more arguments:\n\t%s\n", cmd->str);
                                return EXIT_FAILURE;
                        }
                        index += 2;
                }
        }
        while (index < argc) {
                struct CC_option * opt = CC_has_option(CC, argv[index]);
                if (opt != NULL) {
                        if (!opt->isset)
                                opt->isset = true;
                        else {
                                CC_print_app_error(cc, "You can't use option '%s' twice.\n", argv[index]);
                                return EXIT_FAILURE;
                        }
                }
                else {
                        CC_print_app_error(cc, "Unrecognized option '%s'. See '%s %s'.\n", argv[index], argv[0], cc->help_option.long_str);
                        return EXIT_FAILURE;
                }
                size_t parsed_args = CC_parse_option_arguments(opt, argc - index - 1, argv + index + 1);
                if (parsed_args == -1) {
                        CC_print_app_error(cc, "Option '%s' needs more arguments:\n\t%s, %s\n", argv[index], opt->short_str, opt->long_str);
                        return EXIT_FAILURE;
                }
                index += parsed_args + 1;
        }
        int function_retval = (cmd == NULL ? cc->default_command(CC) : cmd->function(CC));
        return function_retval;
}

bool
cc_isset_option (CCommander *CC,
                 const char *opt_name)
{
        if (*CC == NULL) {
                CC_error(CC_FAILURE_NULL_CC_PASSED);
                return false;
        }
        struct CC_option *opt = CC_has_option(CC, opt_name);
        if (opt == NULL) {
                CC_error(CC_FAILURE_OPTION_NOT_FOUND);
                return false;
        }
        if (opt->isset)
                return true;
        return false;
}

const char*
cc_get_option (CCommander *CC,
               const char *opt_name,
               const char *arg_name)
{
        if (*CC == NULL) {
                CC_error(CC_FAILURE_NULL_CC_PASSED);
                return NULL;
        }
        struct CC_option *opt = CC_has_option(CC, opt_name);
        if (opt == NULL) {
                CC_error(CC_FAILURE_OPTION_NOT_FOUND);
                return NULL;
        }
        if (opt->isset == false)
                return 0;
        for (size_t i = 0; i < opt->argslen; ++i)
                if (strcmp(opt->args[i].name, arg_name) == 0)
                        return opt->args[i].value;
        return 0;
}

const char*
cc_get_command (CCommander *CC,
                const char *cmd_name,
                const char* arg_name)
{
        if (*CC == NULL) {
                CC_error(CC_FAILURE_NULL_CC_PASSED);
                return NULL;
        }
        struct CC_command *cmd = CC_has_command(CC, cmd_name);
        if (cmd == NULL) {
                CC_error(CC_FAILURE_COMMAND_NOT_FOUND);
                return NULL;
        }
        for (size_t i = 0; i < cmd->argslen; ++i)
                if (strcmp(cmd->args[i].name, arg_name) == 0)
                        return cmd->args[i].value;
        return 0;
}

static void
CC_print_help (CCommander *CC)
{
        if (*CC == NULL) {
                CC_error(CC_FAILURE_NULL_CC_PASSED);
                return;
        }
        printf("Usage:\n\t%s <command> <arg1> <arg2> ... [options]\nCommands:\n", (*CC)->binname);
        size_t maxstrlen = 0;
        size_t short_maxstrlen = 0;
        for (size_t i = 0; i < (*CC)->commandslen; ++i) {
                size_t str_len = strlen((*CC)->commands[i].str);
                if (str_len > maxstrlen)
                        maxstrlen = str_len;
        }
        for (size_t i = 0; i < (*CC)->optionslen; ++i) {
                size_t str_len = strlen((*CC)->options[i].long_str);
                size_t short_str_len =  strlen((*CC)->options[i].short_str) ;
                if (str_len > maxstrlen)
                        maxstrlen = str_len;
                if (short_str_len > short_maxstrlen)
                        short_maxstrlen = short_str_len;
        }
        for (size_t i = 0; i < (*CC)->commandslen; ++i)
                printf("\t%-*s%s\n", (int)maxstrlen + (int)short_maxstrlen + 5 + 2, (*CC)->commands[i].str,
                                   (*CC)->commands[i].description);
        printf("Options:\n");
        printf("\t%-*s, %-*s%s\n\t%-*s, %-*s%s\n", (int)short_maxstrlen,
                                           (*CC)->help_option.short_str, (int)maxstrlen + 5,
                                           (*CC)->help_option.long_str,
                                           (*CC)->help_option.description, (int)short_maxstrlen,
                                           (*CC)->version_option.short_str,(int)maxstrlen + 5,
                                           (*CC)->version_option.long_str,
                                           (*CC)->version_option.description);
        for (size_t i = 0; i < (*CC)->optionslen; ++i)
                printf("\t%-*s, %-*s%s\n", (int)short_maxstrlen,
                                       (*CC)->options[i].short_str, (int)maxstrlen + 5,
                                       (*CC)->options[i].long_str,
                                       (*CC)->options[i].description);

}
