#include "ccommander.h"
#include <stdio.h>
#include <stdlib.h>

//int command_setup(CCommander) {}
//int command_exec(CCommander) {}

int setup_command(CCommander * CC) {
    printf("setup command!\n");
    return 0;
}

int exec_command(CCommander * CC) {
        printf("exec command!\n");
        printf("command = %s\n", cc_get_command(CC, "exec", "cmd"));
        printf("arguments = %s\n", cc_get_command(CC, "exec", "args"));
        const char * args2 = cc_get_command(CC, "exec", "args2");
        if (args2)
                printf("arguments = %s\n", args2);
        return 0;
}

int main(int argc, const char** argv) {
    //struct CC_arg * args = NULL;
    //size_t argslen = 0;
    //parse_str_to_args("helloworld <firstname> [lastname] <name> [oops]", &args, &argslen);
    cc_aterror(cc_perror);
	CCommander cc = NULL;
    //cc_set_default_arg_handler(&cc, setup_command);
	cc_version(&cc, "0.0.1");
  cc_name(&cc, "myapp");
 //cc_help_option(&cc, "-H", "--HELP", "HELP ME!");
 //cc_version_option(&cc, "-VI", "--VER", "VER NAZAN!!!");
    const char * options[][3] = {
        {"--chdir <path> <oops> [help] [name]", "-C", "Change the working directory"},
        {"--whole <foo> <bar>", "-W", "Whole new thing!"},
        {"--no-tests [hello]", "-T", "Ignore test hook"}
    };
    cc_options(&cc, 3, options);

    const struct CCommand commands[2] = {
        {"setup [hello]", "It does the setups!", setup_command},
        {"exec <cmd> <args> [args2]", "Runs the given remote command", exec_command}
    };
    cc_commands(&cc, 2, commands);
    cc_set_default_command_alias(&cc, "setup");
    cc_parse(&cc, argc, argv);
    cc_free(&cc);
    return EXIT_SUCCESS;
}
